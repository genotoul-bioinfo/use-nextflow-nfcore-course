![Build Status](https://gitlab.com/pages/mkdocs/badges/master/build.svg)

# Genotoul-bioinfo use nf-core courses

https://genotoul-bioinfo.pages.mia.inra.fr/use-nextflow-nfcore-course/pages/nfcore_running_rnaseq/

## How to Edit

Run the website locally with the following command

```{bash}
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material:9.4.4
```

Open http://localhost:8000 to get a preview.

Now edit/add `.md` files in `docs` dir following [mkdocs](https://www.mkdocs.org) and [mkdocs-material](https://squidfunk.github.io/mkdocs-material) documentations

