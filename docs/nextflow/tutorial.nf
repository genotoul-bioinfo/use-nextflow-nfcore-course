#!/usr/bin/env nextflow
nextflow.enable.dsl = 2
params.greeting = 'Hello world!' 
params.help = false 

def helpMessage() {

   log.info"""
   Usage:
     This workflow contains 2 main steps (called process), the first process 
     splits a string into 6-character chunks, writing each one to a file with 
     the prefix chunk_, and the second receives these files and transforms 
     their contents to uppercase letters.
     
     The typical command for running the pipeline is as follows:
       nextflow run tutorial.nf
    
     Optionnal arguments:
       --greeting "string"     Provide a greeting string.
       --help                   Display this message.

    """
}


if (params.help){
  helpMessage()
  exit 0
}



greeting_ch = Channel.of(params.greeting) 

process SPLITLETTERS { 
    input: 
    val x 

    output: 
    path 'chunk_*' 

    script: 
    """
    printf '$x' | split -b 6 - chunk_
    """
} 


process CONVERTTOUPPER { 
    input: 
    path y 

    output: 
    stdout 

    script: 
    """
    cat $y | tr '[a-z]' '[A-Z]'
    """
} 

workflow { 
    letters_ch = SPLITLETTERS(greeting_ch) 
    results_ch = CONVERTTOUPPER(letters_ch.flatten()) 
    results_ch.view { it } 
} 
