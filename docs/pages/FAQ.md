#FAQ on classical errors
---



??? question "Dependencies not available ?"
    `.command.sh: ligne2: [bowtie2-build or other] : commande introuvable`

    On the Genotoul-Bioinfo platform, commands of workflow are available through singularity.
    To activate it with nf-core, use `-profile genotoul` or add the following lines in `nextflow.config` which is located in
    current directory:
    ```yaml
    singularity {
      enabled = true
      autoMount = true
      runOptions = '-B /bank -B /work -B /save -B /home'
    }
    ```

??? question "Singularity not loaded ?"

    `env: singularity: Aucun fichier ou dossier de ce type`

    You enabled singularity usage but you didn't load the singularity module.
    You should type the following line:

    ```
    module load containers/singularity/3.9.9
    ```

??? question "How to find reason of an error? "

    Their is several way to find the directory associated to the process in error: 

    - Look at log file (`.nextflow.log`), the last process in error is displayed at the end of the file and the directory associated is also displayed.
    - Use the command `nextflow log -f task_id,hash,name,status,exit,duration,realtime,pcpu,pmem [RUN NAME]` 
    - In nfcore pipeline, look at the trace file available in `pipeline_info` directory.

    Then go into the directory of the process 

    - `cd work/c2/8270e1`
    - look at `.command.out` or `.command.log` files

    If the reason of the error is not obvious, you can follow the instruction in the FAQ to "Rerun a command inside a singularity container".


??? question "Files exist but during workflow execution they are not found !"

    ```
    nextflow run nextflow-io/rnaseq
    N E X T F L O W  ~  version 19.10.0
    Launching `nextflow-io/rnaseq` [awesome_bhaskara] - revision: bfcee3841f [master]
    R N A T O Y   P I P E L I N E    
    =============================
    genome: /home/pervenche/.nextflow/assets/nextflow-io/rnaseq/data/ggal/ggal_1_48850000_49020000.Ggal71.500bpflank.fa
    annot : /home/pervenche/.nextflow/assets/nextflow-io/rnaseq/data/ggal/ggal_1_48850000_49020000.bed.gff
    reads : /home/pervenche/.nextflow/assets/nextflow-io/rnaseq/data/ggal/*_{1,2}.fq
    outdir: results
    executor >  local (1)
    executor >  local (1)
    [50/a073c9] process > buildIndex (ggal_1_48850000_49020000.Ggal71.500bpflank) [100%] 1 of 1, failed: 1 ✘
    [-        ] process > mapping                                                 -
    [-        ] process > makeTranscript                                          -
    Error executing process > 'buildIndex (ggal_1_48850000_49020000.Ggal71.500bpflank)'

    Caused by:
      Process `buildIndex (ggal_1_48850000_49020000.Ggal71.500bpflank)` terminated with an error exit status (1)

    Command executed:

      bowtie2-build --threads 1 ggal_1_48850000_49020000.Ggal71.500bpflank.fa genome.index

    Command exit status:
      1

    Command output:
      Settings:
    ...
    Command error:
      Error: could not open ggal_1_48850000_49020000.Ggal71.500bpflank.fa
      Error: Encountered internal Bowtie 2 exception (#1)
      Command: bowtie2-build --wrapper basic-0 --threads 1 ggal_1_48850000_49020000.Ggal71.500bpflank.fa genome.index
    Work dir:
      /work/pervenche/nextflow-tutorial/work/50/a073c950ec60bc12445e7b54c64568
    ```

    If the fasta file exists in the working directory, it should imply that `/work` is not accessible from singularity image.
    Check the config file: does input directory is in line ? `runOptions = '-B /bank  -B /save -B /home'`

    Correction : add the directory with `-B`.
    To find the correct path to add follows the step descript at [II.b](../singularity/testing_nf_img.md)



??? question  "Build a singularity image from a Docker image"
    You may want to build your own image, usually, with nextflow.
    A singularity image is can be build from a Docker one.

    ```
    singularity build rnaseq.sif docker://nextflow/rnatoy:peerj5515
    WARNING: Authentication token file not found : Only pulls of public images will succeed
    INFO:    Starting build...
    Getting image source signatures
    Copying blob sha256:e17be29fd4028456ae0c064c1cc7b0410ee5f5a5b89a44ce375ac64e0b607581
    99.16 MiB / 99.16 MiB [====================================================] 8s
    Copying blob sha256:d2c1575ed524e66b0dfe1d850e25ee31e4053d755d63adec646014726c271772
    ...
    ```

    For Docker, the repository of images is the [docker hub](https://hub.docker.com/search?q=&type=image).
    For singularity, the repository is the [singularity hub](https://singularity-hub.org/).


    > If you want to create it from a recipe you have to do it as root on your computer.




??? question "Rerun a command inside a singularity container"

    If a process crashes and the error message is not explicit, you may want to reproduce the error in order to understand it.
    On the Genotoul-Bioinfo platform, we use nfcore with singularity images. Thus to reproduce, the error you have to enter in the image and execute the command.
    When you use a singularity image and a process fails, you have to run the command inside the container in order to understand the problem.

    If you have an error like :
    ```
    Command error:
      Error: could not open ITAG2.3_genomic_Ch6.fasta
      Error: Encountered internal Bowtie 2 exception (#1)
      Command: bowtie2-build --wrapper basic-0 --threads 1 ITAG2.3_genomic_Ch6.fasta genome.index

    Work dir:
      /work/bleuet/nextflow-tutorial/work/6c/bb527b2ac54e8916eb2fb54f3774ab

    Tip: view the complete command output by changing to the process work dir and entering the command `cat .command.out`
    ```

    To understand the problem, you have to re-execute the command in the same environmment, so go to the work directory of the failed process. Look at files .err or .log.
    1. Try to execute `.command.run`
    ```
    bash .command.run
    ```
    You get the same error... and you get closer to the problem.

    2. Then get the line of singularity command in file `.command.run`
    ```
    $ grep singularity .command.run
    set +u; env - PATH="$PATH" SINGULARITYENV_TMP="$TMP" SINGULARITYENV_TMPDIR="$TMPDIR" SINGULARITYENV_NXF_DEBUG=${NXF_DEBUG:=0} singularity exec -B /bank -B /work -B /save -B /home /work/bleuet/nextflow-tutorial/work/singularity/nextflow-rnatoy@sha256-9ac0345b5851b2b20913cb4e6d469df77cf1232bafcadf8fd929535614a85c75.img /bin/bash -c "cd $PWD; /bin/bash /work/bleuet/nextflow-tutorial/work/6c/bb527b2ac54e8916eb2fb54f3774ab/.command.run nxf_trace"
    ```
    the important part of the line is `singularity exec -B /bank -B /work -B /save -B /home /work/bleuet/nextflow-tutorial/work/singularity/nextflow-rnatoy@sha256-9ac0345b5851b2b20913cb4e6d469df77cf1232bafcadf8fd929535614a85c75.img`

    3. Launch the image in **shell** mode with the options find previously.
    ```
    singularity shell -B /bank -B /work -B /save -B /home <PathToImg>.img
    ```

    4. Execute command
    ```
    sh .command.sh
    ```
    Then the command give you the same error, try to find out what is the problem.
    
    5. Test the access to the input file:
    ```
    ls /usr/local/
    bin  etc  games  include  lib  man  sbin  share  src
    ```
    >!!!! /usr/local/bioinfo/ does not exist in the container !!!!
    >You should add option -B /usr/local/bioinfo/ in nextflow.config.



??? question "For nextflow module, how to know which Java module I have to load ?"
    ```
    module show bioinfo/Nextflow/24.04.2
    ```
    ```
    -------------------------------------------------------------------
    /tools/modulefiles/bioinfo/Nextflow/24.04.2:

    module-whatis   {loads the bioinfo/Nextflow/24.04.2 environment}
    prepend-path    PATH /usr/local/bioinfo/src/Nextflow/Nextflow-v24.04.2/
    setenv          NXF_OPTS -Xmx4G
    -------------------------------------------------------------------
    ```
    Display content of "How_to_use_SLURM_Nextflow" file which is in each directory of all installed software
    ```
    more /usr/local/bioinfo/src/Nextflow/How_to_use_SLURM_Nextflow
    
    ...
    
    -> Version v23.10.0

    #Need Java 17.0.6
    module load devel/java/17.0.6

    module load bioinfo/Nextflow/23.10.0

    -> Version v24.04.2

    #Need Java 17.0.6
    module load devel/java/17.0.6

    module load bioinfo/Nextflow/24.04.2
    
    ...
    
    ```