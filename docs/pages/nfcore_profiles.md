# nf-core profiles
---
## Config and profiles
Nextflow can load pipeline configurations from multiple locations. To make it easy to apply a group of options on the command line, Nextflow uses the concept of [config profiles](https://www.nextflow.io/docs/latest/config.html#config-profiles).  _nf-core_ pipelines load configuration in the following order:

1. Pipeline: Default 'base' config
    * Always loaded. Contains pipeline-specific parameters and "sensible defaults" for things like computational requirements
    * Does _not_ specify any method for software packaging. If nothing else is specified, Nextflow will expect all software to be available on the command line.
2. Pipeline: Core config profiles
    * All _nf-core_ pipelines come with some generic config profiles. The most commonly used ones are for software packaging : `docker`, `singularity` and `conda`
    * Other core profiles are `awsbatch`, `debug` and `test`
3. [nf-core/configs](https://github.com/nf-core/configs): Server profiles
    * At run time, _nf-core_ pipelines fetch configuration profiles from the [configs](https://github.com/nf-core/configs) remote repository. The profiles here are specific to clusters at different institutions. Genotoul profile is available for nf-core pipelines.
4. Local config files given to Nextflow with the `-c` flag
5. Command line configuration

![image](../assets/img/configuration_piority_nf_core.jpg)

Multiple comma-separated config profiles can be specified in one go, so the following commands are perfectly valid:

```
nextflow run nf-core/rnaseq -profile test,singularity
nextflow run nf-core/hlatyping -profile debug,genotoul
```

Note that the order in which config profiles are specified matters. Their priority increases from left to right.

!!! Question "Explore config files of rnaseq pipeline"
    1. Find rnaseq workflow directory
    2. Display the config files present in workflow directory 
        - `nextflow.config` and 
        - `conf/base.config`
    3. Look at the merged config with the command `nextflow config`

??? Solution
    1. `nextflow info nf-core  /rnaseq`
    2. The two files are accessible with :
        - `more /home/$USER/.nextflow/assets/nf-core/rnaseq/nextflow.config`
        - `more /home/$USER/.nextflow/assets/nf-core/rnaseq/conf/base.config`
    2. The merged config is visible with : 
        - `nextflow config nf-core/rnaseq`


## Profile genotoul
With the command `nextflow run nf-core/rnaseq -profile genotoul`, the profile [genotoul.config](https://github.com/nf-core/configs/blob/master/docs/genotoul.md) will be automatically download and use.

!!! warning
    This profile is only available with `nf-core` pipelines.

Multiple comma-separated config profiles can be specified in one go, so the following commands are perfectly valid:
```
nextflow run nf-core/rnaseq -profile test,singularity
nextflow run nf-core/hlatyping -profile debug,genotoul
```

!!! info
    Note that the order in which config profiles are specified matters. Their priority increases from left to right.

## Profile test 
The `test` profile is a bit of a special case. Whereas all other config profiles tell Nextflow how to run on different computational systems, the `test` profile configures each `nf-core` pipeline to run without _any_ other command line flags. It specifies URLs for test data and all required parameters. Thanks to this, you can test any _nf-core_ pipeline.
!!! Question "Test a pipeline with profile genotoul and test"
    The first thing to do before executing a pipeline on your own data is to try with test profile.
        ```
        nextflow run nf-core/rnaseq -profile test,genotoul --outdir results -r 3.14.0
        ```
    