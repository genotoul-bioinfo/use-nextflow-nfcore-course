# Set up the genotoul environement


## Connect to the server
Start your machine and open a terminal (please use [mobaXterm](https://mobaxterm.mobatek.net/download-home-edition.html) for window). You can now try to access the genotoul server by using `ssh`.

```bash
ssh -X $USERNAME@genobioinfo.toulouse.inrae.fr
```

Create tutorial working directory and log in a node
```
cd work
mkdir nextflow-tutorial
cd nextflow-tutorial
srun --mem 4G --pty bash
```

## Creates Nextflow directories

!!! warning
    This step has to be done only one time and set up the home directories for
    nextflow and singularity as on genotoul home dir is limited to 10GB!

On `genobioinfo` execute once the following script which creates two directories :
`~/work/.nextflow` and `~/work/.singularity` and creates symbolic link in home
directory.
```
sh /usr/local/bioinfo/src/NextflowWorkflows/create_nfx_dirs.sh
```

Check that symbolic links exists
```
ls -la ~
```
```
.bashrc
.nextflow -> /home/<username>/work/.nextflow
save -> /save/user/<username>
.singularity -> /home/<username>/work/.singularity
...
work -> /work/user/<username>
```


## Load modules

On `genobioinfo` server, each time you want to use Nextflow you have to load the software module.
Search the available Nextflow modules and load the last version which correspond to ***nextflow***.
```
search_module nextflow
```
```
bioinfo/Nextflow/20.11.0-edge
bioinfo/Nextflow/21.10.6
bioinfo/Nextflow/22.12.0-edge
bioinfo/Nextflow/23.04.3
bioinfo/Nextflow/23.10.0
bioinfo/Nextflow/24.04.2
bioinfo/NextflowWorkflows/nfcore-Nextflow-v20.11.0-edge
bioinfo/NextflowWorkflows/nfcore-Nextflow-v21.10.6
bioinfo/NextflowWorkflows/nfcore-Nextflow-v22.12.0-edge
bioinfo/NextflowWorkflows/nfcore-Nextflow-v23.04.3
bioinfo/NextflowWorkflows/nfcore-Nextflow-v23.10.0
bioinfo/NextflowWorkflows/nfcore-Nextflow-v24.04.2
```

Use the last version of nextflow
```
module load bioinfo/Nextflow/XX.XX.X
module load devel/java/17.0.6
```

??? solution "How to know which Java module I have to load ?"
    ```
    module show bioinfo/Nextflow/24.04.2
    ```
    ```
    -------------------------------------------------------------------
    /tools/modulefiles/bioinfo/Nextflow/24.04.2:

    module-whatis   {loads the bioinfo/Nextflow/24.04.2 environment}
    prepend-path    PATH /usr/local/bioinfo/src/Nextflow/Nextflow-v24.04.2/
    setenv          NXF_OPTS -Xmx4G
    -------------------------------------------------------------------
    ```
    Display content of "How_to_use_SLURM_Nextflow" file which is in each directory of all installed software
    ```
    more /usr/local/bioinfo/src/Nextflow/How_to_use_SLURM_Nextflow
    
    ...
    
    -> Version v23.10.0

    #Need Java 17.0.6
    module load devel/java/17.0.6

    module load bioinfo/Nextflow/23.10.0

    -> Version v24.04.2

    #Need Java 17.0.6
    module load devel/java/17.0.6

    module load bioinfo/Nextflow/24.04.2
    
    ...
    
    ```