


## Rerun, inside a singularity image, a command ...

In case of error, to re-execute the command in same environment as during nextflow execution,
you should execute with the singularity image. 
