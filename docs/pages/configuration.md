# Nextflow config
---

There are different ways to configure nextflow execution: with a configuration file, or with profiles (that could be defined in configuration).

## Use config file

Nextflow looks for a configuration file in the following order, and only
new params are loaded (i.e. the settings in the first file override the same
ones that may appear in the follwing files).

1. in the current directory (e.g. file created previously `nextflow.config`)
2. in the workflow directory (e.g. `~/.nextflow/assets/nextflow-io/rnaseq/nextflow.config`)
3. in the home directory `$HOME/.nextflow/config`
4. finally, if the config file is given with `-c <config file>`, the settings are considered as the first.

![image](../assets/img/configuration_piority.jpg)

> If you want to ignore all configuration files and use only the custom one use the command line option `-C <config file>`.

Configuration file example:
```
#scope by dot prefixing
process.executor = 'slurm'
process.queue = 'workq'
process.memory = '10G'

#scope using the curly brackets
singularity {
  enabled = true
  autoMount = true
}
```

## Use profiles

Configuration files can contain the definition of one or more profiles. A profile is a set of configuration attributes that can be activated/chosen when launching a pipeline execution by using the `-profile` command line option.

```
profiles {

    standard {
        process.executor = 'local'
    }

    cluster {
        process.executor = 'slurm'
        process.memory = '10GB'
    }

    cloud {
        process.executor = 'cirrus'
        process.container = 'cbcrg/imagex'
        docker.enabled = true
    }

}
```

You can check the config which will be used with the command:
```
nextflow config nextflow-io/hello 
```



!!! Question "Understand config"
    * To find the config file of the workflow `nextflow-io/hello` follow thoses steps :
        * Where is stored the downloaded workflow? (*`nextflow info nextflow-io/hello`*)
        * List files contained in `local path`, do you see a config file ?
        * Display the content of this config file.

    * Use the command `nextflow config` in order to see the config again (merged with your current config)
    
    * Rename the file `./nextflow.config` in **current directory** by `nextflow.config.save` (NOT THE CONFIG OF THE WORKFLOW)
    * Execute again the `nextflow config` command, does the configuration is the same? Why?
     
??? Solution
    * nextflow info nextflow-io/hello
    * ls /home/bleuet/.nextflow/assets/nextflow-io/hello
    * more /home/bleuet/.nextflow/assets/nextflow-io/hello/nextflow.config
    * nextflow config nextflow-io/hello
    * mv nextflow.config nextflow.config.save
    * nextflow config nextflow-io/hello. 
    no, because the config from the local file nextflow.config does not exist.  

!!! Question "Understand profile"
    * Create new `nextflow.config` with the [profiles above](#use-profiles)

    * run `nextflow-io/hello` with profile `cluster`. 
    Which executor option is used ?

    * How could you know it before running it?

??? Solution
    * `nextflow run -profile cluster nextflow-io/hello `
    * `nextflow config -profile cluster nextflow-io/hello `
