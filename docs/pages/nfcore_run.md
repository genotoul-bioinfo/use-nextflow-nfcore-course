# nf-core
---

## What is [nf-core](https://nf-co.re/) ?![image](../assets/img/logo/nf-core-logo.svg.png)
* a community
* a set of workflows
* a helper tools



## Loading environment
On `genobioinfo` server, each time you want to use nf-core workflows, you will have to load one of the following module:
```bash
search_module nfcore
```
```
bioinfo/NextflowWorkflows/nfcore-Nextflow-v20.11.0-edge
bioinfo/NextflowWorkflows/nfcore-Nextflow-v21.10.6
bioinfo/NextflowWorkflows/nfcore-Nextflow-v22.12.0-edge
bioinfo/NextflowWorkflows/nfcore-Nextflow-v23.04.3
bioinfo/NextflowWorkflows/nfcore-Nextflow-v23.10.0
bioinfo/NextflowWorkflows/nfcore-Nextflow-v24.04.2
```

The nf-core module depends on the Nextflow module, you can choose the nextflow version you want to use.
```bash
module load bioinfo/NextflowWorkflows/nfcore-Nextflow-v24.04.2
```

!!! Tips
    To know what is contained in a module you can use the following command `module show`
```
module show bioinfo/NextflowWorkflows/nfcore-Nextflow-v24.04.2
-------------------------------------------------------------------
/tools/modulefiles/bioinfo/NextflowWorkflows/nfcore-Nextflow-v24.04.2:

module-whatis   {loads the bioinfo/NextflowWorkflows/nfcore-Nextflow-v24.04.2 environment}
module          load devel/java/17.0.6
module          load containers/singularity/3.9.9
module          load bioinfo/Nextflow/24.04.2
module          load devel/python/Python-3.11.1
setenv          NXF_SINGULARITY_CACHEDIR /usr/local/bioinfo/src/NextflowWorkflows/singularity-img/
-------------------------------------------------------------------
```

This will load :

* Nextflow module,
* singularity module,
* set the path to access to the local singularity images available in `/usr/local/bioinfo/src/NextflowWorkflows/singularity-img/`

## List all available pipeline

### By command line :
```
nf-core list

                                          ,--./,-.
          ___     __   __   __   ___     /,-._.--~\
    |\ | |__  __ /  ` /  \ |__) |__         }  {
    | \| |       \__, \__/ |  \ |___     \`-._,-`-,
                                          `._,._,'

    nf-core/tools version 2.13 - https://nf-co.re
    There is a new version of nf-core/tools available! (2.14.1)


┏━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━┳━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━┓
┃ Pipeline Name             ┃ Stars ┃ Latest Release ┃      Released ┃ Last Pulled ┃ Have latest release?  ┃
┡━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━╇━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━┩
│ sarek                     │   345 │          3.4.2 │   4 weeks ago │           - │ -                     │
│ funcscan                  │    58 │          1.1.5 │  3 months ago │           - │ -                     │
│ scrnaseq                  │   174 │          2.6.0 │   4 weeks ago │           - │ -                     │
│ crisprseq                 │    22 │          2.1.1 │  6 months ago │           - │ -                     │
│ oncoanalyser              │    15 │            dev │     yesterday │           - │ -                     │
│ phaseimpute               │    15 │            dev │     yesterday │           - │ -                     │
│ raredisease               │    74 │          2.1.0 │    1 week ago │           - │ -                     │
│ proteinfold               │    41 │          1.0.0 │   1 years ago │           - │ -                     │
│ bacass                    │    55 │          2.2.0 │  2 months ago │           - │ -                     │
│ bamtofastq                │    13 │          2.1.1 │   4 weeks ago │           - │ -                     │
│ airrflow                  │    44 │          4.1.0 │    4 days ago │           - │ -                     │
│ reportho                  │     4 │            dev │    4 days ago │           - │ -                     │
│ rnavar                    │    31 │          1.0.0 │   2 years ago │ 2 weeks ago │ No (master - 222d7a1) │
```

### On https://nf-co.re/
Each pipeline has its own webpage at `https://nf-co.re/PIPELINE`.
![image](../assets/img/nfcore_rnaseq.png)
See online documentation for [rnaseq pipeline](https://nf-co.re/rnaseq/)

In addition to this documentation, each pipeline comes with basic command line reference. This can be seen by running the pipeline with the `--help` flag, for example:

```
nextflow run nf-core/rnaseq --help
```

!!! note 
    Note that if you run `nextflow run nf-core/rnaseq -help` you will not have the same result !


!!! Question "Rnaseq workflow"
    Visit the page of the workflow [rnaseq ](https://nf-co.re/rnaseq). Check which analyses are performed by default and what are the required parameters ?


!!! Question "Visit your favorite workflow"
    Visit the page of the workflow you are interrested in (https://nf-co.re). 
    Explain : 

      - which analyses are performed ?
      - which input format is expected ?
      - which parameters are required ?
    





## Fetching pipeline code
Unless you are actively developing pipeline code, we recommend using the Nextflow [built-in functionality](https://www.nextflow.io/docs/latest/sharing.html) to fetch _nf-core_ pipelines. Nextflow will automatically fetch the pipeline code when you run `nextflow run nf-core/PIPELINE`. For the best reproducibility, it is good to explicitly reference the pipeline version number that you wish to use with the `-revision`/`-r` flag. For example:

```bash
nextflow run nf-core/rnaseq -revision 3.14.0
```

If not specified, Nextflow will fetch the `master` branch - for _nf-core_ pipelines this will be the latest release. If you would like to run the latest development code, use `-r dev`.

**Note that once pulled, Nextflow will use the local cached version for subsequent runs**. Use the `-latest` flag when running the pipeline to always fetch the latest version. Alternatively, you can force Nextflow to pull a pipeline again using the `nextflow pull` command:

```bash
nextflow pull nf-core/rnaseq
```

!!! Tips
    Get info on this pipeline with `nextflow info` command such as where is store the workflow code?




## Test a workflow 
Each workflow come with test data. The first thing to do, is to try your favorite workflow with option `-profile test`, as you are on genotoul you can add `-profile test,genotoul`. We will see later the content of those profiles. Make sure you are on a node `srun --mem 4G --pty bash` or create a script and run it with sbatch command.
```
$ nextflow run nf-core/rnaseq -profile test,genotoul --outdir results -r 3.14.0

Nextflow 24.04.1 is available - Please consider updating your version to it
N E X T F L O W  ~  version 23.10.0
Launching `https://github.com/nf-core/rnaseq` [lonely_feynman] DSL2 - revision: b89fac3265 [master]
WARN: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Multiple config files detected!
  Please provide pipeline parameters via the CLI or Nextflow '-params-file' option.
  Custom config files including those provided by the '-c' Nextflow option can be
  used to provide any configuration except for parameters.
-profile test,genotoul --outdir results
  Docs: https://nf-co.re/usage/configuration#custom-configuration-files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


------------------------------------------------------
                                        ,--./,-.
        ___     __   __   __   ___     /,-._.--~'
  |\ | |__  __ /  ` /  \ |__) |__         }  {
  | \| |       \__, \__/ |  \ |___     \`-._,-`-,
                                        `._,._,'
  nf-core/rnaseq v3.14.0-gb89fac3
------------------------------------------------------
Core Nextflow options
  revision                  : master
  runName                   : lonely_feynman
  containerEngine           : singularity
  launchDir                 : /work/user/bleuet/nextflow-tutorial
  workDir                   : /work/user/bleuet/nextflow-tutorial/work
  projectDir                : /home/bleuet/.nextflow/assets/nf-core/rnaseq
  userName                  : bleuet
  profile                   : test
  configFiles               : 

Input/output options
  input                     : https://raw.githubusercontent.com/nf-core/test-datasets/7f1614baeb0ddf66e60be78c3d9fa55440465ac8/samplesheet/v3.10/samplesheet_test.csv
  outdir                    : results

Reference genome options
  fasta                     : https://raw.githubusercontent.com/nf-core/test-datasets/7f1614baeb0ddf66e60be78c3d9fa55440465ac8/reference/genome.fasta
  gtf                       : https://raw.githubusercontent.com/nf-core/test-datasets/7f1614baeb0ddf66e60be78c3d9fa55440465ac8/reference/genes_with_empty_tid.gtf.gz
  gff                       : https://raw.githubusercontent.com/nf-core/test-datasets/7f1614baeb0ddf66e60be78c3d9fa55440465ac8/reference/genes.gff.gz
  transcript_fasta          : https://raw.githubusercontent.com/nf-core/test-datasets/7f1614baeb0ddf66e60be78c3d9fa55440465ac8/reference/transcriptome.fasta
  additional_fasta          : https://raw.githubusercontent.com/nf-core/test-datasets/7f1614baeb0ddf66e60be78c3d9fa55440465ac8/reference/gfp.fa.gz
  hisat2_index              : https://raw.githubusercontent.com/nf-core/test-datasets/7f1614baeb0ddf66e60be78c3d9fa55440465ac8/reference/hisat2.tar.gz
  rsem_index                : https://raw.githubusercontent.com/nf-core/test-datasets/7f1614baeb0ddf66e60be78c3d9fa55440465ac8/reference/rsem.tar.gz
  salmon_index              : https://raw.githubusercontent.com/nf-core/test-datasets/7f1614baeb0ddf66e60be78c3d9fa55440465ac8/reference/salmon.tar.gz

Read filtering options
  bbsplit_fasta_list        : https://raw.githubusercontent.com/nf-core/test-datasets/7f1614baeb0ddf66e60be78c3d9fa55440465ac8/reference/bbsplit_fasta_list.txt

UMI options
  umitools_bc_pattern       : NNNN

Alignment options
  pseudo_aligner            : salmon
  min_mapped_reads          : 5

Optional outputs
  save_reference            : true

Process skipping options
  skip_bbsplit              : false

Institutional config options
  config_profile_name       : Test profile
  config_profile_description: The Genotoul cluster profile
  config_profile_contact    : support.bioinfo.genotoul@inra.fr
  config_profile_url        : http://bioinfo.genotoul.fr/

Max job request options
  max_cpus                  : 48
  max_memory                : 120 GB
  max_time                  : 4d

...
...
...

[54/d45a5d] process > NFCORE_RNASEQ:RNASEQ:QUANTIFY_PSEUDO_ALIGNMENT:SE_GENE_SCALED (tx2gene.tsv)                                        [100%] 1 of 1 ✔
[20/af246c] process > NFCORE_RNASEQ:RNASEQ:QUANTIFY_PSEUDO_ALIGNMENT:SE_TRANSCRIPT (tx2gene.tsv)                                         [100%] 1 of 1 ✔
[a5/617edc] process > NFCORE_RNASEQ:RNASEQ:DESEQ2_QC_PSEUDO                                                                              [100%] 1 of 1 ✔
[8a/748204] process > NFCORE_RNASEQ:RNASEQ:CUSTOM_DUMPSOFTWAREVERSIONS (1)                                                               [100%] 1 of 1 ✔
[a0/93c353] process > NFCORE_RNASEQ:RNASEQ:MULTIQC (1)                                                                                   [100%] 1 of 1 ✔
-[nf-core/rnaseq] Pipeline completed successfully -
Completed at: 27-May-2024 19:18:15
Duration    : 6m 3s
CPU hours   : 1.7
Succeeded   : 194

```


!!! success
    Thus you can see if the workflow run correctly on our infrastructure. This is a good news and the first thing to do before trying with you data.


!!! warning "Workflow version"
    Note that the first time you run a workflow you are not obliged to set the version with `-r`.
    But as soon as your pipeline is installed locally, nextflow required it `-r 3.14.0`


