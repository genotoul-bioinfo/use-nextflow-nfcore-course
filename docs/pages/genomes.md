# Handling reference genomes

With nf-core, genomes can be provided througth [iGenome](https://ewels.github.io/AWS-iGenomes/). This options is, by default, de-activated on Genotoul-Bioinfo. It is not recommanded to use on this platform, as it downloads genomes for you in your disk space.

Here are the two way to reuse genomes stored locally : 

* supply reference genome paths by command line via the pipeline's parameters e.g. `--fasta` or `--gtf`.
* by resuming a previous run and adding input data
* complete your configuration `nextflow.config` and store your favorite genomes, as shown here:
```
params {
  genomes {
    'GENOME_NAME' {
      fasta  = '/usr/local/bioinfo/src/NextflowWorkflows/example_on_cluster/data/ITAG2.3_genomic_Ch6.fasta'
      gtf = '/usr/local/bioinfo/src/NextflowWorkflows/example_on_cluster/data/ITAG2.3_genomic_Ch6.gtf'
    }
    'OTHER-GENOME' {
      // [..]
    }
  }
  // Optional - default genome. Ignored if --genome 'OTHER-GENOME' specified on command line
  genome = 'YOUR-ID'
}
```
You will then be able to use the pipeline's parameters `--genome GENOME_NAME`

## Files associated with genomes

The first step is to retrieve all parameters that are associated with a genome. 
By looking at parameters of RNAseq pipeline (for example) you can list all the indexes that can be used [here](https://nf-co.re/rnaseq/3.14.0/parameters#reference-genome-options)

Or 
```
nextflow run nf-core/rnaseq --help
Launching `https://github.com/nf-core/rnaseq` [tender_fermi] DSL2 - revision: b89fac3265 [master]



------------------------------------------------------
                                        ,--./,-.
        ___     __   __   __   ___     /,-._.--~'
  |\ | |__  __ /  ` /  \ |__) |__         }  {
  | \| |       \__, \__/ |  \ |___     \`-._,-`-,
                                        `._,._,'
  nf-core/rnaseq v3.14.0-gb89fac3
------------------------------------------------------
Typical pipeline command:

  nextflow run nf-core/rnaseq --input samplesheet.csv --genome GRCh37 -profile docker

Input/output options
  --input                            [string]  Path to comma-separated file containing information about the samples in the experiment.
  --outdir                           [string]  The output directory where the results will be saved. You have to use absolute paths to storage on Cloud 
                                               infrastructure. 
  --email                            [string]  Email address for completion summary.
  --multiqc_title                    [string]  MultiQC report title. Printed as page header, used for filename if not otherwise specified.

Reference genome options
  --genome                           [string]  Name of iGenomes reference.
  --fasta                            [string]  Path to FASTA genome file.
  --gtf                              [string]  Path to GTF annotation file.
  --gff                              [string]  Path to GFF3 annotation file.
  --gene_bed                         [string]  Path to BED file containing gene intervals. This will be created from the GTF file if not specified.
  --transcript_fasta                 [string]  Path to FASTA transcriptome file.
  --additional_fasta                 [string]  FASTA file to concatenate to genome FASTA file e.g. containing spike-in sequences.
  --splicesites                      [string]  Splice sites file required for HISAT2.
  --star_index                       [string]  Path to directory or tar.gz archive for pre-built STAR index.
  --hisat2_index                     [string]  Path to directory or tar.gz archive for pre-built HISAT2 index.
  --rsem_index                       [string]  Path to directory or tar.gz archive for pre-built RSEM index.
  --salmon_index                     [string]  Path to directory or tar.gz archive for pre-built Salmon index.
  --kallisto_index                   [string]  Path to directory or tar.gz archive for pre-built Kallisto index.
  --hisat2_build_memory              [string]  Minimum memory required to use splice sites and exons in the HiSAT2 index build process. [default: 
                                               200.GB] 
  --gencode                          [boolean] Specify if your GTF annotation is in GENCODE format.
  --gtf_extra_attributes             [string]  By default, the pipeline uses the `gene_name` field to obtain additional gene identifiers from the input GTF file 
                                               when running Salmon. [default: gene_name] 
  --gtf_group_features               [string]  Define the attribute type used to group features in the GTF file when running Salmon. [default: gene_id]
  --featurecounts_group_type         [string]  The attribute type used to group feature types in the GTF file when generating the biotype plot with 
                                               featureCounts. [default: gene_biotype] 
  --featurecounts_feature_type       [string]  By default, the pipeline assigns reads based on the 'exon' attribute within the GTF file. [default: exon]
  ...
```

??? solution "Which parameters could you provide by command line for a genome and its indexes ?"
    * --fasta
    * --gtf
    * --gff
    * --gene_bed
    * --transcript_fasta
    * --additional_fasta
    * --splicesites
    * --star_index
    * --hisat2_index
    * --rsem_index
    * --salmon_index
    * --kallisto_index


    Note that those command line parameters can be added to you config file.

!!! Warning 
    Providing index allows you to avoid regenerating an index and therefore reduces your calculation time
    But if you provide an index, please be sure that it has been generated on same genome and with a software version compatible with the one used in workflow.

!!! Question "Indexes"
    1. Which index have been previously generated ?
    2. Which can you provide by command line ?
    3. Relaunch the previous command rnaseq and provide fasta, rsem_index and gene_bed
    4. Check which index processes are launched (use `nextflow log` command!)

??? solution
    1. 
    ```
    > tree alntomato/genome 
    alntomato/genome/
    ├── genome.transcripts.fa
    ├── index
    │   ├── genome.transcripts.fa
    │   ├── rsem
    │   │   ├── chrLength.txt
    │   │   ├── [...]
    │   │   └── transcriptInfo.tab
    │   └── salmon
    │       ├── complete_ref_lens.bin
    │       ├── [...]
    │       ├── seq.bin
    │       └── versionInfo.json
    ├── ITAG2.3_genomic_Ch6.fasta.fai
    ├── ITAG2.3_genomic_Ch6.fasta.sizes
    ├── ITAG2.3_genomic_Ch6.filtered.bed
    ├── ITAG2.3_genomic_Ch6.filtered.gtf
    └── rsem
        ├── genome.chrlist
        ├── genome.grp
        ├── genome.idx.fa
        ├── genome.n2g.idx.fa
        ├── genome.seq
        ├── genome.ti
        ├── genome.transcripts.fa
        └── ITAG2.3_genomic_Ch6.fasta
    ```
    Note that `alntomato/genome/rsem` and `alntomato/genome/index/rsem` are not the same.

    2. 
      ```
       --rsem_index and --gene_bed
      ```

    3. You could relaunch the workflow and use the following command to reuse rsem_index and gene_bed :
    ```
      nextflow run nf-core/rnaseq -profile genotoul --input sample.csv --fasta ref/ITAG2.3_genomic_Ch6.fasta --outdir alntomato2 --gtf ref/ITAG_pre2.3_gene_models_Ch6.gtf --aligner star_rsem --rsem_index alntomato/genome/index/rsem --gene_bed alntomato/genome/ITAG2.3_genomic_Ch6.filtered.bed -r 3.14.0
    ```

    4. `nextflow log -f task_id,hash,name,status,exit,duration,realtime,pcpu,pmem mad_feynman `

    In the second run the process `NFCORE_RNASEQ:RNASEQ:PREPARE_GENOME:RSEM_PREPAREREFERENCE_GENOME` is not present !

---
## Optionnally to go futher ... 
## re-use indexes generated by nf-core with option --genome ?

### Find genome configuration 

Use command ̀`nextflow config nf-core/rnaseq`

This command displays all parameters...
You can search the genome section

```
params {
...
  genomes {
      GRCh37 {
         fasta = 's3://ngi-igenomes/igenomes/Homo_sapiens/Ensembl/GRCh37/Sequence/WholeGenomeFasta/genome.fa'
         bwa = 's3://ngi-igenomes/igenomes/Homo_sapiens/Ensembl/GRCh37/Sequence/BWAIndex/version0.6.0/'
         bowtie2 = 's3://ngi-igenomes/igenomes/Homo_sapiens/Ensembl/GRCh37/Sequence/Bowtie2Index/'
         star = 's3://ngi-igenomes/igenomes/Homo_sapiens/Ensembl/GRCh37/Sequence/STARIndex/'
         bismark = 's3://ngi-igenomes/igenomes/Homo_sapiens/Ensembl/GRCh37/Sequence/BismarkIndex/'
         gtf = 's3://ngi-igenomes/igenomes/Homo_sapiens/Ensembl/GRCh37/Annotation/Genes/genes.gtf'
         bed12 = 's3://ngi-igenomes/igenomes/Homo_sapiens/Ensembl/GRCh37/Annotation/Genes/genes.bed'
         readme = 's3://ngi-igenomes/igenomes/Homo_sapiens/Ensembl/GRCh37/Annotation/README.txt'
         mito_name = 'MT'
         macs_gsize = '2.7e9'
         blacklist = '/home/bleuet/.nextflow/assets/nf-core/rnaseq/assets/blacklists/GRCh37-blacklist.bed'
      }
      GRCh38 {
        ....
      }
  }
}
```

By this way you get all the indexes parameters name. You are not obliged to fill all the value
```
params{
  genomes {
      GRCh37 {
         fasta = 
         bwa = 
         bowtie2 =
         star =
         bismark =
         gtf =
         bed12 =
         readme =
         mito_name = 'MT'
         macs_gsize = 
         blacklist =
      }
```


### Find genome indexes

When you are running a nf-core pipeline without providing indexes, the pipeline will compute them. In order to avoid to compute them each time you are running the pipeline in a different working directory, you can re-use the results of `results/genome` folder.

Here is an example for rnaseq test workflow:

```
alntomato/genome/
├── genome.transcripts.fa
├── index
│   ├── genome.transcripts.fa
│   ├── rsem
│   │   ├── chrLength.txt
│   │   ├── chrNameLength.txt
│   │   ├── chrName.txt
│   │   ├── chrStart.txt
│   │   ├── exonGeTrInfo.tab
│   │   ├── exonInfo.tab
│   │   ├── geneInfo.tab
│   │   ├── Genome
│   │   ├── genome.chrlist
│   │   ├── genome.grp
│   │   ├── genome.idx.fa
│   │   ├── genome.n2g.idx.fa
│   │   ├── genomeParameters.txt
│   │   ├── genome.seq
│   │   ├── genome.ti
│   │   ├── genome.transcripts.fa
│   │   ├── ITAG2.3_genomic_Ch6.fasta
│   │   ├── Log.out
│   │   ├── SA
│   │   ├── SAindex
│   │   ├── sjdbInfo.txt
│   │   ├── sjdbList.fromGTF.out.tab
│   │   ├── sjdbList.out.tab
│   │   └── transcriptInfo.tab
│   └── salmon
│       ├── complete_ref_lens.bin
│       ├── ctable.bin
│       ├── ctg_offsets.bin
│       ├── duplicate_clusters.tsv
│       ├── info.json
│       ├── mphf.bin
│       ├── pos.bin
│       ├── pre_indexing.log
│       ├── rank.bin
│       ├── refAccumLengths.bin
│       ├── ref_indexing.log
│       ├── reflengths.bin
│       ├── refseq.bin
│       ├── seq.bin
│       └── versionInfo.json
├── ITAG2.3_genomic_Ch6.fasta.fai
├── ITAG2.3_genomic_Ch6.fasta.sizes
├── ITAG2.3_genomic_Ch6.filtered.bed
├── ITAG2.3_genomic_Ch6.filtered.gtf
└── rsem
    ├── genome.chrlist
    ├── genome.grp
    ├── genome.idx.fa
    ├── genome.n2g.idx.fa
    ├── genome.seq
    ├── genome.ti
    ├── genome.transcripts.fa
    └── ITAG2.3_genomic_Ch6.fasta
```

With the names of the files and the directories you can now complete a config file.

Here are the different steps for the result of rnaseq pipeline on Tomato data.

1. Copy genome directory of tomato example in another directory (be sure not to remove it later)
```
mkdir ~/work/allgenomes
cp -r alntomato/genome/ ~/work/allgenomes/tomato/
```

2. Edit `nextflow.config` with parameters  in current directory and add a param genome named tomato.

Example for the tomato pipeline 
```
params {
  genomes {
      tomato {
         fasta = '/home/<USER>/work/tomato/ref/ITAG2.3_genomic_Ch6.fasta'
         fai = '/home/<USER>/work/allgenomes/tomato/ITAG2.3_genomic_Ch6.fasta.fai'
         rsem = '/home/<USER>/work/allgenomes/tomato/index/rsem'
         gtf = '/home/<USER>/work/tomato/ref/ITAG_pre2.3_gene_models_Ch6.gtf'
         bed12 = '/home/<USER>/work/allgenomes/tomato/ITAG2.3_genomic_Ch6.filtered.bed'
         salmon_index = '/home/<USER>/work/allgenomes/tomato/index/salmon'
      }
  }
}
```

!!! Warning
    Be sure that the genome key name you use is not allready used in igenomes (such as GRCh37)

!!! Question "Test genome parameter"
    1. Use `nextflow config` to check if tomato genome is available. 
    1. Remove the genome directory (alntomato/genome/) 
    2. reexecute the previous pipeline on tomato data without `--fasta` and `--gtf` and with option `--genome tomato`.
    
    3. Does Rsem index is generated ?


??? solution 
    1. `nextflow.config` must contain :
    ```
    params {
      genomes {
          tomato {
            fasta = '/home/<USER>/work/tomato/ref/ITAG2.3_genomic_Ch6.fasta'
            fai = '/home/<USER>/work/allgenomes/tomato/ITAG2.3_genomic_Ch6.fasta.fai'
            rsem = '/home/<USER>/work/allgenomes/tomato/index/rsem'
            gtf = '/home/<USER>/work/tomato/ref/ITAG_pre2.3_gene_models_Ch6.gtf'
            bed12 = '/home/<USER>/work/allgenomes/tomato/ITAG2.3_genomic_Ch6.filtered.bed'
            salmon_index = '/home/<USER>/work/allgenomes/tomato/index/salmon'
          }
      }
    }
    ```

    Use command to check if tomato genome is available.
    `nextflow config nf-core/rnaseq -profile genotoul`

    1. `rm -rf alntomato/genome/`

    2. `nextflow run nf-core/rnaseq -profile genotoul --input sample.csv --genome tomato --outdir alntomato --aligner star_rsem -r 3.14.0`

    3. `tree alntomato/genome/index/`
    Only salmon index have been created



