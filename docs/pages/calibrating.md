# Calibrating ressources
---

Nextflow submits jobs with default resources, as defined in the workflow configuration file.
It may be possible that : 

 - jobs are killed due to exceeded memory limit or time limit  
 - tasks are not running because they require too many resources
 - jobs reserve more memory than necessary
 
The default configuration named `base.config` which can be found in `/home/$USER/.nextflow/assets/nf-core/rnaseq/conf/base.config` or with command `nextflow config nf-core/rnaseq` can be overloaded.

By default, settings of nf-core workflows are fixed with a "retry strategy" depending on return errors.
In case the job is killed because it does not have enough CPU, time or memory, you can ask Nextflow to retry using these options :

```
process {
  errorStrategy = { task.exitStatus in ((130..145) + 104) ? 'retry' : 'finish' }
  maxRetries = 1
  maxErrors = '-1'
  
  
  // Process-specific resource requirements
     withLabel:process_high {
      cpus = { check_max( 12    * task.attempt, 'cpus'    ) }
      memory = { check_max( 72.GB * task.attempt, 'memory'  ) }
      time = { check_max( 16.h  * task.attempt, 'time'    ) }
   }
}
```

Genotoul-Bioinfo profile sets the max ressouces :
```
params {
  // Max resources requested by a normal node on genotoul.
  max_memory = 120.GB
  max_cpus = 48
  max_time = 96.h
}
``` 

The user can overload those parameters by creating a file in the current directory called `nextflow.config` or by using the `-c file.conf` option.

For example, if you want to give less memory to all large tasks across most nf-core pipelines, the following config could work:

```
process {
  withLabel:process_high {
    memory = 48.GB
  }
}
```

You can be more specific by targetting a given process name, using `withName`. For example:

```
process {
  withName:bwa_align {
    cpus = 32
  }
}
```

!!! Warning
    It is really important to be as close as possible to you needs in order to properly share the ressources with others users.
    Requiring too much memory may prevent other users from accessing the cluster.
    Requiring too much CPU either!


## Good practice :

* Try to run the pipeline on one sample only, normally we already did it! 
* Then, check the HTML report available in `results/pipeline_info` 
* Calibrate ressources 
* Launch the pipeline on all samples.


!!! question "Execute a pipeline with few ressources"
    1. download a config with few memory :
        ```
        wget https://forgemia.inra.fr/genotoul-bioinfo/use-nextflow-nfcore-course/-/raw/master/docs/conf/nextflow.config
        ```
    1. Add the WT sample in `sample.csv`

    2. Re-launch the pipeline with option `-resume`, in order not to recompute previous analysis and indexes !

    3. Does the pipeline ended correctly ? What happenned ? Copy pipeline_info directory into public_html, and check if logs and ressources required.

    4. The process error RSEM_CALCULATEEXPRESSION ended on an error memory.
    Edit the `nextflow.config` previously downloaded and set memory to 8.GB:
    ```
        process {
        withName:'.*:RSEM_CALCULATEEXPRESSION' {
            memory = 4.GB
        }
        }
    ```
    5. Relaunch the pipeline like point 3 of this exercice.
    6. Does everything ended correctly ?


??? Solution 
    1. Download
    ```
    wget https://forgemia.inra.fr/genotoul-bioinfo/use-nextflow-nfcore-course/-/raw/master/docs/conf/nextflow.config
    ```
    
    1.  samples.csv
    ```
    sample,fastq_1,fastq_2,strandedness
    MT_REP1,data/MT_rep1_1_Ch6.fastq.gz,data/MT_rep1_2_Ch6.fastq.gz,auto
    WT_REP1,data/WT_rep1_1_Ch6.fastq.gz,data/WT_rep1_2_Ch6.fastq.gz,auto
    ```
    2. 
    ```
    nextflow run nf-core/rnaseq -profile genotoul --input sample.csv \
    --rsem_index alntomato/genome/index/rsem \
    --gene_bed alntomato/genome/ITAG2.3_genomic_Ch6.filtered.bed  \
    --outdir alntomato2 --aligner star_rsem -r 3.14.0 -resume
    ```
    3. no, it's ended because, process RSEM_CALCULATEEXPRESSION do not have enougth memory (oom_kill)
    ```
    slurmstepd: error: Detected 1 oom_kill event in StepId=13423144.batch. Some of the step tasks have been OOM Killed.
    ```







## Optionnally ... more exercices

!!! question "Get information about specific processes"
    How many CPU and memory was set to NFCORE_RNASEQ:RNASEQ:PREPARE_GENOME:MAKE_TRANSCRIPTS_FASTA or to NFCORE_RNASEQ:RNASEQ:QUANTIFY_RSEM:RSEM_MERGE_COUNTS ?

??? solution
    Look at table `Tasks` in file `execution_report_XXXX.html`
    
    MAKE_TRANSCRIPTS_FASTA:

     * 12 CPU
     * 72GB
    
    RSEM_MERGE_COUNTS: 

     * 6 CPU
     * 36 GB

!!! question "Get information on process_high"
    How many cpu and memory was set to process_high ?  

??? solution
    ```
    nextflow config nf-core/rnaseq -profile genotoul | grep -A 5  'process_high' 
    ```


!!! question "Decrease by 2 ressources of label `process_high`"
    In order to queue with higher priority, and reduce your carbon impact (less kill/retry), reduce by 2 memory and CPU for label `process_high`. Follow the instructions:
    
    * Edit new file in `nextflow.config` (with `nano`, `gedit` or vi) with following content:
        ``` 
        process {
          withLabel:process_high {
            cpus = { check_max( 6  * task.attempt, 'cpus'    ) }
            memory = { check_max( 36.GB * task.attempt, 'memory'  ) }
          }
        }
        ```
    * Execute `nextflow config nf-core/rnaseq -profile genotoul`
    * Does the process_high values changed ? Why ?


??? solution
    If you followed the previous instruction, no needs of correction.

!!! question "Change value for one process only"
    Add new parameters, set CPU to 1 and memory to 1GB for RSEM_MERGE_COUNTS

??? solution
    ``` 
    process {
      withLabel:process_high {
        cpus = { check_max( 6  * task.attempt, 'cpus'    ) }
        memory = { check_max( 36.GB * task.attempt, 'memory'  ) }
      }
      withName:RSEM_MERGE_COUNTS {
        cpus = { check_max( 1  * task.attempt, 'cpus'    ) }
        memory = { check_max( 1.GB * task.attempt, 'memory'  ) }
      }
    }
    ```

!!! Warning 
    If you want to use check_max() in a custom config file, you must copy the function to the end of your config outside of any configuration scope! It will not be inherited from base.config.
    ```
    // Function to ensure that resource requirements don't go beyond
    // a maximum limit
    def check_max(obj, type) {
        if (type == 'memory') {
            try {
                if (obj.compareTo(params.max_memory as nextflow.util.MemoryUnit) == 1)
                    return params.max_memory as nextflow.util.MemoryUnit
                else
                    return obj
            } catch (all) {
                println "   ### ERROR ###   Max memory '${params.max_memory}' is not valid! Using default value: $obj"
                return obj
            }
        } else if (type == 'time') {
            try {
                if (obj.compareTo(params.max_time as nextflow.util.Duration) == 1)
                    return params.max_time as nextflow.util.Duration
                else
                    return obj
            } catch (all) {
                println "   ### ERROR ###   Max time '${params.max_time}' is not valid! Using default value: $obj"
                return obj
            }
        } else if (type == 'cpus') {
            try {
                return Math.min( obj, params.max_cpus as int )
            } catch (all) {
                println "   ### ERROR ###   Max cpus '${params.max_cpus}' is not valid! Using default value: $obj"
                return obj
            }
        }
    }

    ```
!!! question "Launch workflow on all samples"
    1. Add the WT sample in sample.csv

    2. Re-launch the pipeline with option `-resume`, in order not to recompute previous analysis and indexes !

    3. Copy pipeline_info directory into public_html, and check if new values of memory and CPU were take into account.

??? Solution 
    1.  
    ```
    sample,fastq_1,fastq_2,strandedness
    MT_REP1,data/MT_rep1_1_Ch6.fastq.gz,data/MT_rep1_2_Ch6.fastq.gz,auto
    WT_REP1,data/WT_rep1_1_Ch6.fastq.gz,data/WT_rep1_2_Ch6.fastq.gz,auto
    ```
   
    2. 
    `nextflow run nf-core/rnaseq -profile genotoul --input sample.csv --genome tomato --outdir alntomato --aligner star_rsem -r 3.14.0 -resume`

    3. `cp alntomato


