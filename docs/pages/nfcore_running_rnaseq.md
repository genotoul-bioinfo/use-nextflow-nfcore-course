# Running nf-core rnaseq
---


We are going to configure and launch an RNAseq pipeline with only one sample, for beginning.

Create a new directory 
```
mkdir ~/work/tomato
cd ~/work/tomato
```


We've got Wild type (WT) and Mutant type (MT) of Tomato unstranded RNAseq.

```
mkdir data; cd data
wget http://web-genobioinfo.toulouse.inrae.fr/~formation/19_Rnaseq_Cli/data/reads/MT_rep1_1_Ch6.fastq.gz
wget http://web-genobioinfo.toulouse.inrae.fr/~formation/19_Rnaseq_Cli/data/reads/MT_rep1_2_Ch6.fastq.gz
wget http://web-genobioinfo.toulouse.inrae.fr/~formation/19_Rnaseq_Cli/data/reads/WT_rep1_1_Ch6.fastq.gz
wget http://web-genobioinfo.toulouse.inrae.fr/~formation/19_Rnaseq_Cli/data/reads/WT_rep1_2_Ch6.fastq.gz
ls 
cd ~/work/tomato
```

As a reference we are going to use the chromosome 6 as an exemple. Download the fasta (sequence) and gtf (annotation) files.
```
mkdir ref; cd ref;
wget http://web-genobioinfo.toulouse.inrae.fr/~formation/19_Rnaseq_Cli/data/reference/ITAG2.3_genomic_Ch6.fasta
wget http://web-genobioinfo.toulouse.inrae.fr/~formation/19_Rnaseq_Cli/data/reference/ITAG_pre2.3_gene_models_Ch6.gtf
cd ~/work/tomato
```

!!! Question "Create the input sample file"
    Now create the samples file with **mutant only** inorder to test the pipeline.
    
    Format example :
    ```
    sample,fastq_1,fastq_2,strandedness
    CONTROL_REP1,AEG588A1_S1_L002_R1_001.fastq.gz,AEG588A1_S1_L002_R2_001.fastq.gz,auto
    CONTROL_REP1,AEG588A1_S1_L003_R1_001.fastq.gz,AEG588A1_S1_L003_R2_001.fastq.gz,auto
    CONTROL_REP1,AEG588A1_S1_L004_R1_001.fastq.gz,AEG588A1_S1_L004_R2_001.fastq.gz,auto
    ```

??? Solution "Solution sample.csv"
    ```
    sample,fastq_1,fastq_2,strandedness
    MT_REP1,data/MT_rep1_1_Ch6.fastq.gz,data/MT_rep1_2_Ch6.fastq.gz,auto
    ```


!!! Question "Preparing the command line"

    === "Beginner"
        We want to align samples on Chr6 of tomato, with STAR and use RSEM for quantification and write result into `alntomato`
        Find the good parameters by visiting [rnaseq page](https://nf-co.re/rnaseq/3.14.0/parameters).


        If you are not on a node, please logging to a node, to test the command line.
        ```
        srun --mem 4G --pty bash
        ```
        Then test the line until you've got no errors and the workflow starts.
    
    === "Advance"
    
        As soon as you've got the beginner step running, you can try to launch the same command throw a script. An example is given at the end of this page.
    

??? Solution 
    ```
    nextflow run nf-core/rnaseq -profile genotoul \
    --input sample.csv --fasta ref/ITAG2.3_genomic_Ch6.fasta \
    --outdir alntomato --gtf ref/ITAG_pre2.3_gene_models_Ch6.gtf 
    --aligner star_rsem -r 3.14.0
    ```






!!! Question "Explore pipeline_info directory"

    1. When the pipeline is completed, explore files:
        - with mobaXterm on windows
        - or with your public_html directory
            - from the frontal node (on compute node you don't have writing permision on save directory)
            - copy the html files from `work/pipeline_info` into your `public_html`.

    2. Could you see how much memory was allocated for process PREPARE_GENOME?
    3. What is the average % of CPU use or % of MEM for processes  ?
   
---
## Launch the main process with a script


Usually, when you handle a lot of samples, the main Nextflow proccess has to be launched on unlimitq with a least 4G of memory.
Here is an example of a script `myscript.sh` to launch with sbatch command.

=== "MySimpleScript.sh"
    ```
    #!/bin/bash
    module purge
    module load bioinfo/NextflowWorkflows/nfcore-Nextflow-v24.04.2

    nextflow run nf-core/rnaseq -profile genotoul --input sample.csv \
    --fasta ref/ITAG2.3_genomic_Ch6.fasta \
    --outdir alntomato --gtf ref/ITAG_pre2.3_gene_models_Ch6.gtf \
    --aligner star_rsem -r 3.14.0

    ```

    To submit the job, use the sbatch command line as following:

    ```
    sbatch --mem=6G -p unlimitq MySimpleScript.sh
    ```


=== "MyFullScript.sh"
    ```
    #!/bin/bash
    #SBATCH -J nfcore-rnaseq
    #SBATCH -p unlimitq
    #SBATCH --mem=6G
    #SBATCH --mail-type=BEGIN,END,FAIL
    #SBATCH --mail-user=<USER@INSTITUTE>

    module purge
    module load bioinfo/NextflowWorkflows/nfcore-Nextflow-v24.04.2

    nextflow run nf-core/rnaseq -profile genotoul --input sample.csv \
    --fasta ref/ITAG2.3_genomic_Ch6.fasta \
    --outdir alntomato --gtf ref/ITAG_pre2.3_gene_models_Ch6.gtf \
    --aligner star_rsem -r 3.14.0

    ```

    To submit the job, use the sbatch command line as following:

    ```
    sbatch MyFullScript.sh
    ```


