## Find error source
---

In case of error, you should **read** the error, they are usually explicit, and follow Nextflow indications, such as:

```
[0;35m[nf-core/rnaseq] Pipeline completed with errors
Error executing process > 'star (WT_rep1)'

Caused by:
  Process `star (WT_rep1)` terminated with an error exit status (137)

Command executed:

  STAR --genomeDir star \
      --sjdbGTFfile ITAG2.3_genomic_Ch6.gtf \
      --readFilesIn WT_rep1_1_Ch6_val_1.fq.gz WT_rep1_2_Ch6_val_2.fq.gz  \
      --runThreadN 10 \
      --twopassMode Basic \
      --outWigType bedGraph \
      --outSAMtype BAM SortedByCoordinate --limitBAMsortRAM 4194967296 \
      --readFilesCommand zcat \
      --runDirPerm All_RWX  \
      --outFileNamePrefix WT_rep1_1_Ch6 --outSAMattrRGline ID:WT_rep1_1_Ch6 'SM:WT_rep1_1_Ch6'

  samtools index WT_rep1_1_Ch6Aligned.sortedByCoord.out.bam

Command exit status:
  137

Command output:
  Aug 07 12:22:00 ..... started STAR run
  Aug 07 12:22:00 ..... loading genome
  Aug 07 12:22:01 ..... processing annotations GTF
  Aug 07 12:22:01 ..... inserting junctions into the genome indices
  Aug 07 12:22:07 ..... started 1st pass mapping
  Aug 07 12:22:34 ..... finished 1st pass mapping
  Aug 07 12:22:34 ..... inserting junctions into the genome indices

Command error:
  .command.sh: line 11: 194540 Killed                  STAR --genomeDir star --sjdbGTFfile ITAG2.3_genomic_Ch6.gtf --readFilesIn WT_rep1_1_Ch6_val_1.fq.gz WT_rep1_2_Ch6_val_2.fq.gz --runThreadN 10 --twopassMode Basic --outWigType bedGraph --outSAMtype BAM SortedByCoordinate --limitBAMsortRAM 4194967296 --readFilesCommand zcat --runDirPerm All_RWX --outFileNamePrefix WT_rep1_1_Ch6 --outSAMattrRGline ID:WT_rep1_1_Ch6 'SM:WT_rep1_1_Ch6'

Work dir:
  /work/bleuet/nextflow-tutorial/work/0e/0ffacdf04fd04c5ec3860509da1119

Tip: when you have fixed the problem you can continue the execution adding the option `-resume` to the run command line

```

* Go to the work directory
* Try to reproduce the error. To do so, you should follows steps describe in [section II.b](../singularity/testing_nf_img.md).
 * Enter in the image
 * Launch `.command.sh`
 * Find the problem
 * Look at [FAQ](../FAQ.md), which references classical errors
* Change configuration or input to solve the problem

In some case, the problem can come from the pipeline.
Note that bugs of pipelines are referenced in Github issues, such as the [rnaseq pipeline](https://github.com/nf-core/rnaseq/issues).
