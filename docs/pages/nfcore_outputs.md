# nf-core outputs
---

Nextflow creates a `work` directory as we allready see [here](/use-nextflow-nfcore-course/pages/nextflow_outputs).

Most of nf-core pipeline require  `--outdir` parameters to define the results directory.
So the previous command ` nextflow run nf-core/rnaseq -profile test,genotoul --outdir results` generate thoses files.

```bash
$ ls -la
drwxr-xr-x   5 bleuet BIOINFO   4096 May 27 19:37 .
drwxr-x---   6 bleuet BIOINFO   4096 May 22 11:26 ..
drwxr-xr-x   4 bleuet BIOINFO   4096 May 27 19:35 .nextflow         => internal files
-rw-r--r--   1 bleuet BIOINFO    562 May 27 19:11 nextflow.config   => config file
-rw-r--r--   1 bleuet BIOINFO  22692 May 27 19:35 .nextflow.log     => log file with all
-rw-r--r--   1 bleuet BIOINFO    108 May 27 19:35 .nextflow.log.1
drwxr-xr-x  10 bleuet BIOINFO   4096 May 27 19:18 results              => final results
drwxr-xr-x 140 bleuet BIOINFO   8192 May 27 19:17 work              => working and temporary files
```

## `outdir` directory
Depending on the executed workflow the results files will be organized differently.
You can have an overview of what files are generated on each pipeline page.

Like here for RNAseq pipeline :

 * Description of outputs : [description](https://nf-co.re/rnaseq/3.14.0/docs/output)
 * Example of outputs : [example](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066)

## `work` directory

The nf-core pipeline can generate a huge amount of large file in working directory.
It is important at the end of a finished run to clean this directory.


!!! warning
    Check disk space with `du -sh *`

    Do not forget to clean directory with `nextflow clean`


!!! Question 
    Find the working directory for `STAR_ALIGN (WT_REP1)` and look at the parameters used for Star in .command.sh

??? Solution
    * grep "STAR_ALIGN (WT_REP1)" results/pipeline_info/execution_trace_2024-06-11_15-24-51.txt
    * more work/6b/f656021f147960e73004a60cdb1408/.command.sh 
    * 
    ```
    STAR \
    --genomeDir star \
    --readFilesIn input1/WT_REP1_primary_1.fastq.gz input2/WT_REP1_primary_2.fastq.gz \
    --runThreadN 2 \
    --outFileNamePrefix WT_REP1. \
     \
    --sjdbGTFfile genome_gfp.gtf \
    --outSAMattrRGline 'ID:WT_REP1'  'SM:WT_REP1'  \
    --quantMode TranscriptomeSAM --twopassMode Basic --outSAMtype BAM Unsorted --readFilesCommand zcat --runRNGseed 0 --outFilterMultimapNmax 20 --alignSJDBoverhangMin 1 --outSAMattributes NH HI AS 
    NM MD --quantTranscriptomeBan Singleend --outSAMstrandField intronMotif
    ```