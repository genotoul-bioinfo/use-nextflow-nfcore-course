#Use nf-core workflows ![genotoul](./assets/img/logo/bioinfo_logo-rvb-petit.png)
---
Last update: 06/11/2024

<object data="./slides/slidesNextFlowNfcore.pdf" type="application/pdf" style="width: 700px;
  height: 400px;">
    <embed src="./slides/slidesNextFlowNfcore.pdf" type="application/pdf" />
</object>


## What is _nf-core_? ![image](./assets/img/logo/nf-core-logo.svg.png) !
[_nf-core_](https://nf-co.re/) is a community-led project to develop ***a set of best-practice pipelines*** built using Nextflow. A suite of helper tools aims to help people run and develop pipelines. Actually, ***59 pipelines*** are available for different application such as RNAseq, BSseq, Hi-C, annotation...
It can be used on any computer or cluster without large prerequisites. It's based on nextflow.


## What is Nextflow?  ![image](./assets/img/logo/nextflow2014.png)
[Nextflow](https://www.nextflow.io) is :

- a programming language that is designed to manage computational workflows
- and a software that can run a workflow on any infrasctructure.

So, it enables scalable and reproducible scientific workflows using software containers.
Nextflow is developed by the Comparative Bioinformatics group at the Barcelona Centre for Genomic Regulation (CRG).

All workflow software dependencies can be embedded into a container.

![image](./assets/img/environment.png)

## What are containers?  ![image](./assets/img/logo/singularity.png)
A container is a standard unit of software that packages up code and all its dependencies, so that the application runs quickly and reliably from one computing environment to another.
[Singularity](https://sylabs.io) and Docker are containers system. On HPC such as Genotoul, you can only use Singularity containers.

## What this tutorial will cover ?
This tutorial attempts to give an overview of how to use Nextflow command line and _nf-core_ pipelines with singularity: how to run _nf-core_ pipelines, how to understand execution errors, how to configure pipelines, where are my results, how to monitor?


## What this tutorial will NOT cover ?
This is NOT a bioinformatic training on a particular workflow. You will NOT learn how to develop a workflow.

## Where to get help ?

* ***about genotoul cluster***:  
    - Follow the Genotoul-Bioinfo Unix and cluster [training slides](https://genotoul-bioinfo.pages.mia.inra.fr/linux-cluster/).
    - Look at our [Bioinfo FAQ](http://bioinfo.genotoul.fr/index.php/faq/bioinfo_tips_faq/) or [Job submission FAQ](http://bioinfo.genotoul.fr/index.php/faq/job_submission_faq/).
    - Ask for help with this [form](http://bioinfo.genotoul.fr/index.php/ask-for/support/).


* ***about nf-core pipelines***

    The beauty of _nf-core_ is that there are a lot of help on offer! The main place for this is Slack - an instant messaging service. The _nf-core_ Slack organization has channels dedicated for each pipeline, as well as specific topics (_eg._ `#new-pipelines`, `#tools` and `#aws`).

    The _nf-core_  Slack can be found at [https://nfcore.slack.com](https://nfcore.slack.com/) (_NB: no hyphen in `nfcore`!_). To join you will need an invite, which you can get at [https://nf-co.re/join/slack](https://nf-co.re/join/slack).

---

